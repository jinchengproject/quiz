Solutions are found inside the respective folders:

[Pb82/html/form.html](/Pb82/html/form.html)  
[Pb82/html/style.css](/Pb82/html/style.css)

[Pb82/js/show-posts.html](/Pb82/js/show-posts.html)

One note for the JS assignment:

Because the certificate of the page/server hosted on amazon is self signed. When you lanuch the webpage on the browser, You have to tweak the settings of the browser to accept display website/ data coming from a self signed source.

---

# Hiring for 2020

This project holds evaluation exercises for hiring technical lead and software enginner roles for the year 2020.

### Please attempt the following assessment in any order of your choosing

1. Create a form found in html folder with instructions given in the folder

1. Retrieve and display posts from a blog with instructions given in the folder

1. Troubleshoot to find out the reason for the pipeline failure
