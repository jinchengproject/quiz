Because the certificate of the page/server hosted on amazon is self signed. When you lanuch the webpage on the browser, You have to tweak the settings of the browser to accept display website/ data coming from a self signed source.

---

## Use JavaScript or a langugage of your choice to connect to a RESTful API and process JSON output in a responsive manner

#### Connect to the following endpoint to retrieve Posts

https://ec2-13-250-253-71.ap-southeast-1.compute.amazonaws.com/wp-json/wp/v2/posts

#### Connect to the following endpoint to retrieve Users

https://ec2-13-250-253-71.ap-southeast-1.compute.amazonaws.com/wp-json/wp/v2/users

#### Update show-posts.html to display posts using the following format

1. Title
1. URL to Post
1. Date Created
1. Author Name
1. Content
