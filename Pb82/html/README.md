## Use Bootstrap v4 to create a responsive form

#### Update form.html to recreate a sign up form found in the URL below.

1. https://www.singaporeglobalnetwork.gov.sg/keep-in-touch/

1. You only need to implement the form with similar validation logic.

1. Please refer to the hi-fi design mockup named hi-fi-form.png for placement of form fields.

1. You may disregard other elements of the web page like top navigation or social media sharing buttons.

1. You may also disregard the vector graphics in the hi-fi design mockup.
